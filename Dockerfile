# syntax=docker/dockerfile:1
FROM php:8.1-fpm

# Install system dependencies
RUN apt-get update && apt-get install -y \
    openssh-client \
    git \
    mariadb-client \
    jq \
    unzip \
    curl \
    && docker-php-ext-install mysqli pdo pdo_mysql

# SSH Key setup
RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan -p 7999 -H gitlab.cern.ch >> ~/.ssh/known_hosts

# Install Composer globally
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer --version

# Copy custom scripts and app content to the image
COPY ./root/usr/share/container-scripts/php/ /usr/share/container-scripts/php/

# Make scripts executable
RUN chmod +x /usr/share/container-scripts/php/pre-start/*

# Set working directory
WORKDIR /var/www/html

# Install dependencies at build time
COPY ./root/usr/share/container-scripts/php/app/composer.json ./composer.json
COPY ./root/usr/share/container-scripts/wp-cli.yml ./wp-cli.yml

# Do not run Composer as root/super user! See https://getcomposer.org/root for details
# Set up drupal minimum stack
ENV COMPOSER_MEMORY_LIMIT=-1
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN --mount=type=ssh composer install --optimize-autoloader -v

# Copy mu plugins
COPY ./root/usr/share/container-scripts/php/wordpress-content/mu-plugins/* wordpress/wp-content/mu-plugins/

# Symlink WP CLI
RUN ln -s /var/www/html/vendor/wp-cli/wp-cli/bin/wp /usr/bin/wp

RUN useradd -ms /bin/bash wpuser \
    && chown -R wpuser:wpuser /var/www/html

USER wpuser

ENTRYPOINT ["/usr/share/container-scripts/php/pre-start/entrypoint.sh"]



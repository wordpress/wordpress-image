###################  Docker development helpful directives  ####################
#
# Usage:
# make logs                   # displays log outputs from running services
# make build 		          # build images
# make up                     # create and start containers
# make destroy                # stop and remove containers, networks, images, and volume
# make reload                 # destroy and restart containers, networks, images, and volume
# make shell              	  # start bash inside service
# make down              	  # stop services
# env						  # build + shell

up:
	docker compose up -d --remove-orphans
.PHONY: up

build:
	docker compose build --no-cache --parallel
.PHONY: build

logs:
	docker compose logs -f
.PHONY: logs

destroy: down
	docker compose rm -f
.PHONY: destroy

down:
	docker compose down --volumes
.PHONY: down

reload: destroy build up shell
.PHONY: reload

shell:
	docker compose exec wordpress /bin/bash
.PHONY: shell

env: up shell
.PHONY: env

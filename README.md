# WordPress image

This project is the WordPress skeleton that will be consumed by the [https://gitlab.cern.ch/wordpress/paas/wordpress-operator](https://gitlab.cern.ch/wordpress/wordpress-operator).

# WordPress Lite Theme

The WordPress Lite Theme is contained in the image created from this [https://gitlab.cern.ch/wordpress/wordpress-image](https://gitlab.cern.ch/wordpress/wordpress-image) repo.

## Design

This repository provides a Dockerized setup for running WordPress with PHP-FPM. The Dockerfile installs necessary tools like MariaDB client, Composer, and WP-CLI, and includes custom scripts to handle tasks such as setting up WordPress, configuring plugins (e.g., OpenID Connect Generic), and managing file permissions. The `docker-compose.yml` file links WordPress with MySQL and NGINX, creating a fully functional local environment. All custom scripts, themes, and configurations are organized under `root/usr/share/container-scripts/php` for easy management. This design makes it straightforward to customize, extend, and deploy WordPress efficiently.

## Development

###  Build

We depend on CERN private repositories at build time, therefore make sure you have a gitlab.cern.ch ssh key configured locally and then run

```bash
eval $(ssh-agent)
ssh-add ~/.ssh/id_rsa # needs to match your key name
```

before running docker compose.

### Run

To start the project
```
make reload
```

In order to run locally https can be disabled with
```
wp config set FORCE_SSL_ADMIN false --raw
wp option set siteurl http://localhost
wp option set home http://localhost
```

To run locally with OIDC we need to:

1. Create an app-portal registration
Use http; and save the client id and client secret
![1.png](docs%2F1.png)
![2.png](docs%2F2.png)

Configure role administrator at least.

2. Configure /etc/hosts in our local machines
To do that simply add:
```
127.0.0.1 carina-dev-wordpress.web.cern.ch
```

3. Configure wp CLI missing options. For example for website `carina-dev-wordpress.web.cern.ch`:

```
wp option patch insert openid_connect_generic_settings client_id <client-id>
wp option patch insert openid_connect_generic_settings client_secret <client-secret>
wp config set FORCE_SSL_ADMIN false --raw
wp option set siteurl http://carina-dev-wordpress.web.cern.ch
wp option set home http://carina-dev-wordpress.web.cern.ch

# recommended
wp config set WP_DEBUG true --raw

```

Finally, go to on your preferred browser http://carina-dev-wordpress.web.cern.ch/wp-login.php

worker_processes auto;

events {
    worker_connections 1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout 65;

    server {
        listen 80;
        server_name localhost;

        root /var/www/html/wordpress;
        index index.php index.html;

        # Deny access to vi swap files
        location ~ /\.swp$ {
            deny all;
        }

        # Deny access to XML-RPC endpoint
        location = /xmlrpc.php {
            deny all;
        }

        # Disable PHP execution in wp-content/uploads
        location ~* /wp-content/uploads/.*\.php$ {
            deny all;
        }

        # Handle directory indexing and disable autoindex
        location / {
            autoindex off;
            try_files $uri $uri/ /index.php?$args;
        }

        # Pass PHP requests to the WordPress container (PHP-FPM)
        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_pass wordpress:9000;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }

        # Deny access to hidden files and folders
        location ~ /\.(?!well-known).* {
            deny all;
        }
    }
}

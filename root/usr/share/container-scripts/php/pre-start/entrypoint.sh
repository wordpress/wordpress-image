#!/bin/bash

FLAG_FILE="/var/www/html/.installed"

# Wait for the installation flag file
echo "Waiting for WordPress installation to complete..."
while [ ! -f "$FLAG_FILE" ]; do
    sleep 2
done

echo "Installation detected. Checking for updates..."

wp core update --allow-root && wp core update-db --allow-root
wp config set FORCE_SSL_ADMIN true --raw

if ! grep -q "if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && strpos( \$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false ) \$_SERVER['HTTPS'] = 'on';" wordpress/wp-config.php; then
    sed -i "/'FORCE_SSL_ADMIN', *true *);/a\
    if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && strpos( \$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false ) \$_SERVER['HTTPS'] = 'on';" wordpress/wp-config.php
fi

# Refresh hostname configs
wp option set siteurl https://$APPLICATION_NAME
wp option set home https://$APPLICATION_NAME
wp option set blogname $APPLICATION_NAME
wp option set title $APPLICATION_NAME
wp option set admin_email $ADMIN_EMAIL


echo "Starting PHP-FPM..."
# Start PHP-FPM
php-fpm

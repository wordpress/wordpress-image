#!/bin/bash

FLAG_FILE="/var/www/html/.installed"

# Configure OpenID Connect Generic Plugin settings
configure_oidc_plugin() {
    echo "--> Configuring OIDC Connect Generic Plugin..."

    # Initialize settings if not already present
    wp option get openid_connect_generic_settings --allow-root > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        # Initialize settings as an empty JSON object if they don't exist
        wp option add openid_connect_generic_settings '{}' --format=json --allow-root
    fi
    # passing args through pipe due to misfire of has_stdin in wp option patch when running in s2i scripts
    # see more https://github.com/wp-cli/entity-command/issues/164
    # piping can be removed if s2i in dropped

    # Configure plugin settings
    wp option patch insert openid_connect_generic_settings client_id "${OIDC_CLIENT_ID}" --allow-root
    wp option patch insert openid_connect_generic_settings client_secret "${OIDC_CLIENT_SECRET}" --allow-root
    wp option patch insert openid_connect_generic_settings login_type 'auto' --allow-root
    wp option patch insert openid_connect_generic_settings scope 'openid' --allow-root
    wp option patch insert openid_connect_generic_settings endpoint_login "${AUTHZ_API_ENDPOINT}/protocol/openid-connect/auth" --allow-root
    wp option patch insert openid_connect_generic_settings endpoint_userinfo "${AUTHZ_API_ENDPOINT}/protocol/openid-connect/userinfo" --allow-root
    wp option patch insert openid_connect_generic_settings endpoint_token "${AUTHZ_API_ENDPOINT}/protocol/openid-connect/token" --allow-root
    wp option patch insert openid_connect_generic_settings endpoint_end_session "${AUTHZ_API_ENDPOINT}/protocol/openid-connect/logout" --allow-root
    wp option patch insert openid_connect_generic_settings link_existing_users 1 --allow-root
    wp option patch insert openid_connect_generic_settings create_if_does_not_exist 1 --allow-root

    echo "--> OIDC Connect Generic Plugin configured."
}

# Wait for the database to be ready
wait_for_db() {
    echo "Waiting for database at $MYSQL_HOST..."
    until wp db check; do
        sleep 3
    done
    echo "Database is ready!"
}

# Create config file, --skip-check avoids the need for mysql binary and mysql lcoal install
echo "--> Creating Wordpress configuration files ..."
wp config create --dbname="$MYSQL_DATABASE" --dbhost="$MYSQL_HOST:$MYSQL_PORT" --dbprefix="$MYSQL_TABLE_PREFIX" --dbuser="$MYSQL_USER" --dbpass="$MYSQL_PASSWORD" --skip-check

# Ensure the database is ready
wait_for_db

wp core is-installed --allow-root

wp_install_status=$?
# Check if WordPress is installed
if [ $wp_install_status -eq 1 ]; then
    echo "WordPress not installed. Installing..."
    wp core install --url=https://$APPLICATION_NAME --title=$APPLICATION_NAME --admin_user=admin --admin_email=$ADMIN_EMAIL --skip-email

    # Enable english language
    wp site switch-language en_US

    wp plugin delete hello

    # Activate Plugins and Themes
    configure_oidc_plugin
    # later should be replaced by entry in wp-config.php define('WP_DEFAULT_THEME', 'cern-lite');
    wp theme activate cern-lite

    # See https://developer.wordpress.org/advanced-administration/security/https/#using-a-reverse-proxy
    # If WordPress is hosted behind a reverse proxy that provides SSL, but is hosted itself without SSL, these options will initially send any requests into an infinite redirect loop. To avoid this, you may configure WordPress to recognize the HTTP_X_FORWARDED_PROTO header (assuming you have properly configured the reverse proxy to set that header).
    wp config set FORCE_SSL_ADMIN true --raw
    # // in some setups HTTP_X_FORWARDED_PROTO might contain
    # // a comma-separated list e.g. http,https
    # // so check for https existence
    if ! grep -q "if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && strpos( \$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false ) \$_SERVER['HTTPS'] = 'on';" wordpress/wp-config.php; then
        sed -i "/'FORCE_SSL_ADMIN', *true *);/a\
        if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && strpos( \$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false ) \$_SERVER['HTTPS'] = 'on';" wordpress/wp-config.php
    fi
    echo "WordPress configuration completed."
fi

# Create flag file to indicate installation is complete
touch "$FLAG_FILE"
echo "Installation complete. Flag file created."

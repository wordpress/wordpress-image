<?php
/*
Plugin Name: CERN Roles
Description: Maps OpenID Connect roles to WordPress roles. Once enabled, this role mapper will enforce permissions configured at the Application-Portal. By end of November this plugin will be enforced for all websites. Action user is required, please see instructions at our https://wordpress.docs.cern.ch
Version: 1.0.0
Plugin URI: https://wordpress.docs.cern.ch/
Author: CERN Infrastructure
*/

function oidc_cern_role_assignment($user = null) {
    if (!$user) {
        $user = wp_get_current_user();
    }

    if (!$user || !isset($user->user_login)) {
        error_log("Unable to determine username.");
        return;
    }

    write_log_debug("User object: " . print_r($user, true));

    // Get ID Token Claim from user meta
    $id_token_claim = get_user_meta($user->ID, "openid-connect-generic-last-id-token-claim", true);

    // Check if cern_roles exists in the claim
    if (!isset($id_token_claim["cern_roles"])) {
        error_log("cern_roles not found in ID Token Claim");
        return;
    }

    $cern_roles = $id_token_claim["cern_roles"];
    write_log_debug("Claim object: " . print_r($cern_roles, true));

    // Define a mapping between CERN roles and WordPress roles
    $role_mapping = [
        "administrator" => "administrator",
        "editor" => "editor",
        "author" => "author",
        "contributor" => "contributor",
    ];

    // Assign WordPress roles based on CERN roles
	foreach ($cern_roles as $cern_role) {
        if (isset($role_mapping[$cern_role])) {
            $wp_role = $role_mapping[$cern_role];
            if (!in_array( $wp_role, (array) $user->roles )) {
                $user->add_role($wp_role);
                write_log_debug("Assigned WordPress role '$wp_role' based on CERN role '$cern_role'");
            }
        }
    }
    # Remove roles not in claim
    foreach($user->roles as $role) {
        if(!in_array($role, (array) $cern_roles)){
    	    $user->remove_role($role);
    	    write_log_debug("Removed role '$role'");
    	}
    }

    if (empty($user->roles)) {
        error_log("No roles found.");
        return;
    }

    write_log_debug("Assigned WordPress roles: " . implode(", ", $user->roles));

    # update name and username
    $new_name = isset($id_token_claim["name"]) ? $id_token_claim["name"] : $user->display_name;
    $new_upn = isset($id_token_claim["cern_upn"]) ? $id_token_claim["cern_upn"] : $user->user_login;
    $user_data = wp_update_user( array( 'ID' => $user->ID, 'nickname' => $new_upn, 'display_name' => $new_name ) );
    if ( is_wp_error( $user_data ) ) {
    	// There was an error; possibly this user doesn't exist.
        error_log("Unable to update user data.");
    }
}

// Hook into the OpenID Connect Generic plugin's user logged-in event
add_action("openid-connect-generic-user-logged-in", "oidc_cern_role_assignment");

<?php
/*
Plugin Name: CERN Lite
Author: CERN Infrastructure
Description: This plugin adds necessary CERN Infrastructure dependencies.
Version: 2.0.0
Plugin URI: https://wordpress.docs.cern.ch/
*/

const ACTION_DEACTIVATE = 'deactivate';

const DISABLED_PLUGIN_ACTIONS = [
    "Disable REST API"     => array(ACTION_DEACTIVATE),
    "OpenID Connect Generic" => array(ACTION_DEACTIVATE),
    "Jetpack Boost" => array(ACTION_DEACTIVATE),
    "FileBird Lite" => array(ACTION_DEACTIVATE)
];

/**
 * Adds the WordPress Lite Notice
 */
function wpb_admin_notice_info() {
echo '<div class="notice notice-info ">
           <p><b>CERN WordPress Lite is out!</b></p>
           <p>See more details at <a href="https://wordpress.docs.cern.ch">https://wordpress.docs.cern.ch!</a>.</p>
           </div>';
}
add_action( 'admin_notices', 'wpb_admin_notice_info' );

/**
 * Adds the WordPress App-Catalogue Deprecation Notice
 */
function wpb_admin_notice_warn() {
echo '<div class="notice notice-error ">
           <p><b>CERN WordPress is coming!</b></p>
           <p><b>As a preventive measure and in preparation for the upcoming offering of a centrally managed WordPress Service at CERN, installing additional plugins as been disabled.</b></p>
           <p>This measure allows to facilitate the future migration from WordPress in app-catalogue to the future centrally managed WordPress.
           <br>Re-enabling plugin installation is heavily frowned upon and will lead to disqualifying from an assisted migration to the future WordPress offering at CERN.
           <br>Websites with new plugins installed after March 1, 2024 will not be offered an assisted migration.</p>
           </div>';
}
add_action( 'admin_notices', 'wpb_admin_notice_warn' );

/**
 * Debug logs via error_log
 */
function write_log_debug($data) {
    if (WP_DEBUG === true) {
        if (is_array($data) || is_object($data)) {
            $data_str = print_r($data, true);
            error_log("DEBUG: $data_str \n");
            return;
        }

        error_log("DEBUG: $data \n");
    }
}


/**
 * Disable plugin actions
 */
add_filter('plugin_action_links', 'disable_plugin_actions', 10, 4);
function disable_plugin_actions( $actions, $plugin_file, $plugin_data = []) {
	$name = array_key_exists('Name', $plugin_data) ? $plugin_data["Name"] : "";

	if (!array_key_exists($name, DISABLED_PLUGIN_ACTIONS)) {
		return $actions;
	}

    foreach (DISABLED_PLUGIN_ACTIONS[$name] as $disabled_action) {
    	if (array_key_exists( $disabled_action, $actions )) {
        	write_log_debug("Disabling '$disabled_action' for plugin '$name");
        	unset( $actions[$disabled_action] );
    	}
    }

    return $actions;
}


/**
 * Remove bulk plugin actions
 */
add_filter( 'bulk_actions-plugins', 'remove_plugins_bulk_actions' );
function remove_plugins_bulk_actions( $actions ){
	// remove all actions
	return array();
}

## CERN Lite Theme

We currently do not fetch automatically from https://gitlab.cern.ch/wordpress/theme-lite.
The current theme version was added from the master branch, tag `1.2.0-beta`
